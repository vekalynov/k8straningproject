FROM openjdk:11-jre-slim
ADD ./jar/dummy_microservice dummy_microservice.jar
EXPOSE 8080
CMD ["java","-jar","dummy_microservice.jar"]
