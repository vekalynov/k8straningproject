package com.dummy;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DummyApplicationTest {
	private static final String HOST = "http://localhost";
	private static final String REST_CONTROLLER = "hello/";

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void testGetHelloMessage() {
		String restResponse = this.restTemplate.getForObject(HOST + ":" + port + "/" + REST_CONTROLLER,
				String.class);
		assertThat(restResponse).contains("Hello");
	}

	@Test
	void testGetMessage() {
		final String message = "My message";
		HttpEntity<String> request = new HttpEntity(message);
		String restResponse = this.restTemplate.postForObject(HOST + ":" + port + "/" + REST_CONTROLLER + "message",
				request, String.class);
		assertThat(restResponse).contains(message);
	}
}