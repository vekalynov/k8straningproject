package com.dummy.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloWorld {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getHelloMessage() {
        return ResponseEntity.ok()
                             .body("Hello World!!!");
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public ResponseEntity getMessage(@RequestBody String message) {
        return ResponseEntity.ok()
                             .body("This is your message: " + message);
    }

}
